### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.


### Show embed in readme

https://gitlab.com/jivanvl/test-node-project/environments/711946/metrics?start=2019-08-20T17%3A28%3A07.000Z&end=2019-08-20T17%3A58%3A07.000Z&group=System%20metrics%20(Kubernetes)&title=Core%20Usage%20(Pod%20Average)&y_label=Cores%20per%20Pod
